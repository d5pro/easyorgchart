// These search functions are unused until I can determine a clean
// way to get site_url() into this file
function searchKeyPress(e)
{
    // look for window.event in case event isn't passed in
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
        document.getElementById('button_search').click();
    }
}

function search(elname)
{
    query = document.getElementById('text_search').value;
	window.location =  '/index.php/search/' + encodeURI(query);
}
