<?php
class Org extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('meta_model');
		$this->meta['updated_on'] = $this->meta_model->get_updated();

		$this->load->model('employees_model');
	}

	public function index()
	{
		$data['employees'] = $this->employees_model->get_org();
		$data['typeahead'] = $this->get_typeahead();
		
		$this->load->view('templates/header', $this->meta);
		$this->load->view('org/index', $data);
		$this->load->view('templates/footer', $this->meta);
	}

	public function id($emp_id)
	{
		$data['employee'] = $this->employees_model->get_employee($emp_id);
		if (empty($data['employee']))
		{
			show_404();
		}
		$data['employees'] = $this->employees_model->get_org($emp_id);

		$data['managers'] = $this->employees_model->get_managers($emp_id);		

		if (count(array($data['managers'])) > 0)
		{
			$data['manager'] = $data['managers'][count(array($data['managers']))-1];
		}

		$this->load->view('templates/header', $this->meta);
		$this->load->view('org/index', $data);
		$this->load->view('templates/footer', $this->meta);
	}

	public function search($query = "")
	{
		$data['employees'] = $this->employees_model->search($query);
		$data['typeahead'] = $this->get_typeahead();
		
		if (count($data['employees']) == 1)
		{
			$this->load->helper('url');
			
			$e = $data['employees'][0];
			redirect(site_url('/org/id/'.$e['id']), 'location');
			return;
		}
		
		$this->load->view('templates/header', $this->meta);
		$this->load->view('search/index', $data);
		$this->load->view('templates/footer', $this->meta);
	}

	public function get_typeahead()
	{
		$employees = $this->employees_model->get_all_employees();
		
		$jsary = array();
		foreach ($employees as $e)
		{
			$name = $e['fname'] . ' ' . $e['lname'];
			
			array_push($jsary, $name);
		}		

		$js = join(',', $jsary);
		$js = preg_replace("/'/", "\\'", $js);
		$js = preg_replace('/"/', '\\"', $js);
		$js = preg_replace('/,/', '","', $js);
		$js = '["' . $js . '"]';
		
		return $js;
	}

}
