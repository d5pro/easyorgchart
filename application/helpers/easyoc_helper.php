<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// http://www.if-not-true-then-false.com/2010/php-1st-2nd-3rd-4th-5th-6th-php-add-ordinal-number-suffix/
function getOrdinalNumberSuffix($num) {

    if (!in_array(($num % 100), array(11,12,13)))
	{
      	switch ($num % 10)
 		{
	        // Handle 1st, 2nd, 3rd
	        case 1:  return 'st';
	        case 2:  return 'nd';
	        case 3:  return 'rd';
      	}
    }
    return 'th';
}

function getEmployeeLink($emp_id, $name)
{
	$url = site_url('/org/id/' . $emp_id);
	return "<a href=\"$url\">$name</a>";
}