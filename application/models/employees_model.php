<?php
class Employees_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->initialize();
	}

	public function initialize()
	{
		$this->load->model('meta_model');
		if ($this->meta_model->get_initialized() == FALSE)
		{
			$this->set_preorder_tree_traversal_values();
			$this->meta_model->set_initialized(TRUE);
		}	
	}
	
	public function get_org($emp_id = 1)
	{
		$emp = $this->get_employee($emp_id);
		if ($emp)
		{
			// Minimize the data returned.
			$query = $this->db->select() //'id, fname, lname, depth')
								->from('employee')
								->where('trav_left >=', $emp['trav_left']) // = include the root
								->where('trav_left <', $emp['trav_right'])
								->order_by('trav_left')
							  	->get();
			return $query->result_array();
		}
		return NULL;
	}

	public function get_employee($emp_id)
	{
		if ($emp_id > 0)
		{
			$query = $this->db->select()
								  	->from('employee')
									->where('id =', $emp_id)
								  	->get();
			$rows = $query->result_array();
			if (count($rows) == 1)
			{
				return $rows[0];
			}
		}
		return NULL;
	}
	
	public function get_managers($emp_id)
	{
		$managers = array();
		if ($emp_id > 0)
		{
			log_message('error', $emp_id);
			do {
				$query = $this->db->select()
									  	->from('employee')
										->where('id =', $emp_id)
									  	->get();
				$rows = $query->result_array();				
				if (count($rows) == 1)
				{
					$emp_id = $rows[0]['managerid'];
					array_unshift($managers, $rows[0]);
				}
			} while (count($rows) == 1);
		}

		return $managers;
	}
	
	public function search($searchstr)
	{
		if ($searchstr != NULL and $searchstr != "")
		{
			$searchstr = urldecode($searchstr);
			// Prepare query string
			// $searchstr = preg_replace('/\s{1,}/', '%', $searchstr);
			
			$query = $this->db->select()
								  	->from('employee')
									->like("CONCAT(fname,' ',lname)", $searchstr, FALSE) // FALSE prevents escaping wildcards
									->or_like('phone', $searchstr, FALSE)
									->order_by('lname','fname')
								  	->get();
			
			return $query->result_array();
		}
		return NULL;
	}
	

	public function get_all_employees()
	{
		$query = $this->db->select('id, fname, lname')
							->from('employee')
							->order_by('fname', 'lname')
						  	->get();
		return $query->result_array();
	}

	public function set_preorder_tree_traversal_values($emp_id = 1, $trav_left = 1, $depth = 0)
	{
		$count = $trav_left;
		
		# Make sure the employee ID is real and the traversal hasn't already been done.
		$query = $this->db->select('id')
							->from('employee')->where('id', $emp_id)->get();
		$rows = $query->result_array();
		if (count($rows) == 0)
		{
			return $count;
		}
		
		# Get Employee Direct Reports, if any
		$query = $this->db->select('id')
							->from('employee')
							->where('managerid', $emp_id)
							->order_by('lname')->order_by('fname')
							->get();
		$rows = $query->result_array();
		if (count($rows) > 0)
		{
			foreach ($rows as $row) 
			{
				$count = $this->set_preorder_tree_traversal_values($row['id'], $count+1, $depth+1);				
			}
		}
				
		# Update Employee Information
		$trav_right = $count + 1;
		$data = array('trav_left' => $trav_left, 'trav_right' => $trav_right, 'depth' => $depth);
		$this->db->where('id', $emp_id);
		$this->db->update('employee', $data);	

		return $trav_right;
	}
}