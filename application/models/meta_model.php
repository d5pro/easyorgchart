<?php
class Meta_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_meta()
	{
		$query = $this->db->select()
							->from('meta')
							->order_by('updated_on')
						  	->get();
		$rows = $query->result_array();
		return (count($rows) == 1) ? $rows[0] : NULL;
	}

	public function get_updated()
	{
		$meta = $this->get_meta();
		if ($meta == NULL) 
		{
			// This is not mission critical, so return something but make it obviously wrong.
			return date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, 1970));
		}
		return $meta['updated_on'];
	}

	public function get_initialized()
	{
		$meta = $this->get_meta();
		if ($meta == NULL) 
		{
			// This will make it so that the left/right initialization
			// is always run. This is a bad thing.
			return FALSE;
		}
		return $meta['initialized'];
	}

	public function set_initialized()
	{
		$data = array('initialized' => '1');

		// There should only be one record, but update them all just incase
		$this->db->update('meta', $data);

		return;
	}
}

