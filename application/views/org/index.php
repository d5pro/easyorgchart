<?php
$this->load->helper('html');

//=================================================================================================
// Create Org Tree
//

// All of this fancy indentation may seem overkill to you,
// but it sure makes it easy to see when your missing LI and UL markers
$start_depth = -1;
$prev_depth = -1;
$tot_depth = 0;
$uplevel_id = 0;
$first = true;
$treehtml = "";
$info = "";
foreach ($employees as $e)
{
	// Initialize
	if ($start_depth < 0)
	{
		$uplevel_id = (isset($employee)) ? $employee['managerid'] : 0;
		$start_depth = $e['depth'];
		$prev_depth = -1;
		$treehtml .= "\n<ul class='tree top depth-0'>\n";	
		$first = true;
	}
	
	$depth = $e['depth'] - $start_depth;
	$tot_depth = ($depth > $tot_depth) ? $depth : $tot_depth;
	$indent = 4 * $depth;
	if ($indent > 0)
	{
		// How much are we indenting to make it pretty
		//   Each LI is indented 4 (2 for the parent UL and 2 for the LI)
	
		// Going down ...
		if ($prev_depth < $depth)
		{
			$treehtml .= "\n";       // Newline after previous unclosed LI
			$treehtml .= str_repeat(' ', $indent-2) . "<ul class='depth-".$depth."'>\n";   // Open new UL
		}
		// Coming back up ...
		elseif ($prev_depth > $depth)
		{
			$treehtml .= "</li>\n";                               // Close previous LI
			for ($d = $prev_depth; $d > $depth; $d--)
			{
				$this_indent = 4 * ($d);

				$treehtml .= str_repeat(' ', $this_indent-2) . "</ul>\n";  // Close previous ULs
				$treehtml .= str_repeat(' ', $this_indent-4) . "</li>\n";    // Close parent LI				
			}
		}
		// Staying level ...
		else // $prev_depth === $depth
		{
			$treehtml .= "</li>\n";  // Close previous LI
		}
	}
	$emp_id = 'emp_' . $e['id']; 
	$li_class = ($first) ? "class='top'" : "";
	$treehtml .= str_repeat(' ', $indent)
			. "<li id='" . $emp_id . "' " . $li_class . ">";
	
	// Don't make the root person link if we are showing their information
	if ($first and isset($employee))
	{
		$treehtml .= $e['lname'] . ', ' . $e['fname'];
	}
	else
	{
		$name = $e['fname'] . ' ' . $e['lname'];
		$data = $e['title'];
		$data .= (isset($e['phone'])) ? '<br/>Phone: ' . $e['phone'] : "";
		$data .= (isset($e['mobile'])) ? '<br/>Mobile: ' . $e['mobile'] : "";
		$data .= (isset($e['location'])) ? '<br/>Office: ' . $e['location'] : "";
 		$data = htmlspecialchars($data, ENT_QUOTES);
		$treehtml .= "<a id='" . $e['id'] . "' rel='tooltip' title='".$name
					."' data-content='".$data."' href='" . site_url("/org/id/" . $e['id']) . "'>"
					. $e['lname'] . ', ' . $e['fname'] . "</a>";		
	}
	
	$first = false;
	$prev_depth = $depth;
}

$treehtml .= "</li>\n";  // Close previous LI
for ($d = $prev_depth; $d > 0; $d--)
{
	$this_indent = 4 * ($d);

	$treehtml .= str_repeat(' ', $this_indent-2) . "</ul>\n";  // Close previous ULs
	$treehtml .= str_repeat(' ', $this_indent-4) . "</li>\n";    // Close parent LI				
}
$treehtml .= "</ul>\n";  // Close final UL 

// End Org Tree Creation
//=================================================================================================
?>
<?php
	$n_mgrs = (isset($managers)) ? count($managers) : 0;
	if ($n_mgrs > 0)
	{
		echo "<ul class='breadcrumb'>";
		for ($i = 0; $i < $n_mgrs; $i++)
		{
			$mname = $managers[$i]['fname'] . ' ' . $managers[$i]['lname'];
			if ($i == $n_mgrs - 1) // The last breadcrumb
			{
				echo "<li class='active'>$mname</li>";
			} else {
				echo "<li>" . getEmployeeLink($managers[$i]['id'], $mname) . "<span class='divider'>&rarr;</span></li>";				
			}
		}
		echo "</ul>";
	}
?>

<div class="row">
  <div class="span12">
	<div class="btn-toolbar">
<?php if ($tot_depth > 1) { ?>
	  <div class="btn-group" data-toggle="buttons-radio">
		<button class="btn" disabled>Show:</button>
<?php
	$named_levels = array("All", "Directs");
	for ($d = 1; $d < $tot_depth; $d++)
	{
		if (isset($named_levels[$d]))
		{
			$name = $named_levels[$d];
		} else {
			$name = "$d<sup>" . getOrdinalNumberSuffix($d) . "</sup> Level";			
		}
		$level = ($d == 0) ? $tot_depth : $d;
		echo <<<EO_BUTTON
			<button class="btn" onClick="toggleLevels('$level');">$name</button>
EO_BUTTON;
		$active_str = "";
	}
	?>
	    <button class="btn active" onClick="toggleLevels('<?php echo $tot_depth; ?>');">All</button>
      </div> <!-- btn-group -->
<?php } ?>
    </div> <!-- btn-toolbar -->
  </div> <!-- span12 -->
</div> <!-- row -->

<div id="content" class="row">
<?php if (count($employees) > 1) {?>
  <div class="span6">
  <?php echo $treehtml; ?>
  </div>
<?php } ?>
  <div class="span6">
	<div id="info">
<?php
	// Create employee table
	if (isset($employee)) {
		$ename = $employee['fname'] . ' ' . $employee['lname'];
	?>
		
	<table class='table table-condensed table-striped'>
		<thead><tr><th colspan=2><?php echo $ename?></th></tr></thead>
		<tbody>
		<tr><td>Title:</td><td><?php echo $employee['title'] ?></td></tr>
		<tr><td>Phone:</td><td><?php echo $employee['phone'] ?></td></tr>
<?php if ($employee['mobile'] != "") { ?>
		<tr><td>Mobile:</td><td><?php echo $employee['mobile'] ?></td></tr>
<?php } ?>
		<tr><td>Email:</td><td><a href="mailto:<?php echo $employee['email'] ?>"><?php echo $employee['email'] ?></a></td></tr>
		<tr><td>Office:</td><td><?php echo $employee['location'] ?></td></tr>
		<tr><td>Department:</td><td><?php echo $employee['department'] ?></td></tr>
		<tr><td>Dept. Id:</td><td><?php echo $employee['departmentid'] ?></td></tr>
<?php	if (isset($manager)) {
		$mname = $manager['fname'] . ' ' . $manager['lname'];
?>
		<tr><td>Manager:</td><td><a href="<?php echo site_url('/org/id/' . $manager['id']); ?>"><?php echo $mname ?></a></td></tr>
<?php   } ?>
		</tbody>
	</table>
<?php } ?>
	</div> <!-- info -->
  </div> <!-- span6 -->
</div> <!-- row -->

<script type="text/javascript">
function goUpLevel()
{
	window.location = "<?php echo site_url("/org/id/" . $uplevel_id); ?>";
}

function toggleLevels(show_depth)
{
	max_depth = <?php echo $tot_depth; ?>;
	for (i = 0; i <= max_depth; i++)
	{
		tag = 'ul.tree ul.depth-' + i;
		if (i <= show_depth)
		{
			$(tag).show();			
		}
		else
		{
			$(tag).hide();						
		}
	}
}

$(function () {	
    $('[rel=tooltip]').popover();  
	$('ul.tree li:last-child').addClass('last');
	$('ul.tree li.top').removeClass('last');
	$('ul.tree').addClass('treepix');
});
</script>
