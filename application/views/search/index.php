<?php
$results = "<div id='search'><ul class='tree top'>";
if ($employees)
{
	foreach ($employees as $e)
	{
		$emp_id = 'emp_' . $e['id']; 
		$results .= "<li id='" . $emp_id . "' class='top'>" 
					. "<a id='" . $e['id'] . "' href='" . site_url("/org/id/" . $e['id']) . "'>" . $e['lname'] . ', ' . $e['fname'] . "</a>";
	}
}
else
{
	$results .= "<li>No search results found.</li>\n";
}
$results .= "</ul>\n</div>";  // Close final UL 
?>

<div id="leftcol" class="<?php if (count($employees) > 100) { echo "minheight"; } ?>">
  <h3>Search Results</h3>
  <?php echo $results; ?>
</div> <!-- leftcol -->
