// These search functions need to be parsed due to site_url() so I pull them
// in as a template into the header template
function searchKeyPress(e)
{
    // look for window.event in case event isn't passed in
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
        document.getElementById('button_search').click();
    }
}

function search()
{
    var query = document.getElementById('txtSearch').value;
    var frmSearch = document.getElementById('frmSearch');

	frmSearch.action = '<?php echo site_url('/search') ?>/' + escape(query);
	frmSearch.submit();
}