<div id="footer" class="clearfix">
	<p class="update">Last updated on <?php echo $updated_on ?></p>
	<p class="attrib">Powered by <a href="https://bitbucket.org/davfive/easy-orgchart" target="_blank">Easy OrgChart</a>, Icons by <a href="http://glyphicons.com" target="_blank">glyphicons</a> (<a href="http://creativecommons.org/licenses/by/3.0/deed.en" target="_blank">CC</a>)</p>
</div> <!-- footer -->
</div> <!-- wrap -->
</body></html>