<?php
 echo "<!DOCTYPE html>";
 echo "<head><title>". $this->config->item('site_name') ."</title>";
 echo meta('Content-type', 'text/html; charset=utf-8', 'equiv');
 echo jquery('1.7.2');
 echo css('../bootstrap/css/bootstrap.css');	
 //echo css('../bootstrap/css/bootstrap-responsive.min.css');	
 echo js("../bootstrap/js/bootstrap.min.js");
 echo js("bootstrap-button.js");
 echo js("bootstrap-tooltip.js");
 echo js("bootstrap-popover.js");
 echo css('site.css'); 
 echo js('easyoc.js');
?>
<script type="text/javascript">
<?php $this->load->view('templates/easyoc_js'); ?>
</script>
</head>
<body>
<div class="container">
<div class="page-header row-fluid">
  <div class="span8">
	<h1><?php echo $this->config->item('site_name'); ?></h1>
  </div>
  <div class="span4">
	<form id="frmSearch" class="form-search pull-right" action="javascript:search();">
	 	<div class="input-append">
			<input type="text" id="txtSearch" class="input-medium search-query" accesskey="s"><button type="submit" class="btn"><i class='icon-search'></i> Search</button>
		</div>
    </form>
  </div>
</div>
